# GGM scan (gluino neutralino grid)

import pyslha

## Parameter values
mu_min = 1000
mu_max = 2000
mu_step = 100

M1_min = 1000
M1_max = 2000
M1_step = 100

M3_min = 1000
M3_max = 2000
M3_step = 100

# Construct vectors
s_M1 = range(M1_min, M1_max, M1_step)
s_mu = range(mu_min, mu_max, mu_step)
s_M3 = range(M3_min, M3_max, M3_step)
s_tanb = [1.5, 5., 10., 25., 50.]

s_Msq = [2.5E+03,]
s_Gmass = [1E-09,]
s_AT = [0,]
s_M2 = [3E+03]

def filter_slha_fn(slha_path):
    masses = pyslha.read(slha_path).blocks['MASS']
    m_gl = masses[1000021]
    m_N1 = masses[1000022]
    if m_N1 > m_gl:
        return True

    return False
